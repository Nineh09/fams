﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
	public class Unit : BaseEntity
	{
		[Key]
		public int UnitID { get; set; }

		[StringLength(255)]
		public string UnitName { get; set; }

		[StringLength(255)]
		public string Description { get; set; }

		public int NumberLesson { get; set; }

		public int SyllabusID { get; set; }

		[ForeignKey("SyllabusID")]
		public Syllabus Syllabus { get; set; }
		public ICollection<Lesson> Lessons { get; set; }
		public ICollection<Attendance> Attendances { get; set; }

	}
}
