﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
	public class Training : BaseEntity
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ProgramID { get; set; }

		[StringLength(100)]
		public string TrainingName { get; set; }

		public DateTime TrainingDate { get; set; }

		[StringLength(100)]
		public string CreatedBy { get; set; }

		public DateTime CreatedDate { get; set; }

		[StringLength(50)]
		public string Duration { get; set; }

		[StringLength(50)]
		public string Status { get; set; }

		public ICollection<ProgramDetails> ProgramDetails { get; set; }
	}
}
