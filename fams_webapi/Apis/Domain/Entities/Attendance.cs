﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
	public class Attendance : BaseEntity
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int AttendanceID { get; set; }

		public DateTime Date { get; set; }

		public int UserID { get; set; }

		public int ClassID { get; set; }

		public int UnitID { get; set; }

		[StringLength(100)]
		public string Status { get; set; }

		[ForeignKey("UserID")]
		public User User { get; set; }

		[ForeignKey("ClassID")]
		public Class Class { get; set; }

		[ForeignKey("UnitID")]
		public Unit Unit { get; set; }
	}

}
