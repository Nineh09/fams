﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
	public class Class : BaseEntity
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ClassId { get; set; }

		[StringLength(100)]
		public string ClassName { get; set; }

		[StringLength(50)]
		public string ClassCode { get; set; }

		public DateTime CreatedOn { get; set; }

		[StringLength(100)]
		public string? CreatedBy { get; set; }

		[StringLength(50)]
		public string? Duration { get; set; }

		[StringLength(100)]
		public string? Location { get; set; }

		[StringLength(10)]
		public string? FSU { get; set; }

		public string? ClassTime { get; set; }

		[StringLength(100)]
		public string? LocationDetail { get; set; }

		[StringLength(100)]
		public string? Admin { get; set; }

		[StringLength(100)]
		public string? Trainer { get; set; }

		public int ProgramID { get; set; }

		[ForeignKey("ProgramID")]
		public Training Program { get; set; }

		public ICollection<Attendance> Attendances { get; set; }

		public ICollection<ClassStudents> ClassStudents { get; set; }
	}
}
