﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
	public class ProgramDetails : BaseEntity
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ProgramDetailId { get; set; }

		public int SyllabusID { get; set; }

		public int ProgramID { get; set; }

		[StringLength(50)]
		public string? CreatedBy { get; set; }

		[StringLength(50)]
		public string? Sequence { get; set; }

		[ForeignKey("ProgramID")]
		public Training Program { get; set; }

		[ForeignKey("SyllabusID")]
		public Syllabus Sysllabus { get; set; }
	}
}
