﻿using Application.Interfaces;
using Application.Services;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Http.Features;
using System.Diagnostics;
using System.Reflection;
using WebAPI.Authorization;
using WebAPI.Middlewares;
using WebAPI.Services;
using WebAPI.Validations;
using WebAPI.Validations.UserValidationModels;

namespace WebAPI
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddWebAPIService(this IServiceCollection services)
        {
            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();
            services.AddHealthChecks();
            services.AddSingleton<GlobalExceptionMiddleware>();
            services.AddSingleton<PerformanceMiddleware>();
            services.AddSingleton<Stopwatch>();

  
            services.AddValidatorsFromAssemblyContaining<UserRegisterModelValidation>(); // any validator will works
            services.AddFluentValidationAutoValidation(); // the same old MVC pipeline behavior
            services.AddFluentValidationClientsideAdapters(); // for client side
            

            services.AddScoped<IClaimsService, ClaimsService>();
			services.AddScoped<IEmailService, EmailService>();
			services.AddScoped<IJwtUtils, JwtUtils>();
            services.AddHttpContextAccessor();
            services.AddFluentValidationAutoValidation();
            services.AddFluentValidationClientsideAdapters();
            services.Configure<FormOptions>(options =>
            {
                options.MultipartBodyLengthLimit = long.MaxValue;
                options.ValueLengthLimit = int.MaxValue;
                options.MultipartHeadersLengthLimit = int.MaxValue;
            });
            return services;
        }
    }
}
