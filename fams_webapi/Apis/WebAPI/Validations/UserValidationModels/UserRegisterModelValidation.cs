﻿using Application.Utils;
using Application.ViewModels.UserViewModels;
using FluentValidation;

namespace WebAPI.Validations.UserValidationModels
{
    public class UserRegisterModelValidation : AbstractValidator<UserRegisterModel>
    {

        public UserRegisterModelValidation() 
        {
            RuleFor(user => user.Email).NotNull().NotEmpty().Matches(RegexUtils.GetMailRegex()).WithMessage("Not a valid Gmail.");
            RuleFor(user => user.Password).NotEmpty().Length(5,50).WithMessage("Should be between 5 to 50 characters.");
            RuleFor(user => user.DateOfBirth).NotEmpty().NotNull().LessThan(DateTime.Today.AddDays(1)).WithMessage("Must be a valid date.");
            RuleFor(user => user.FullName).NotNull().NotEmpty().Length(4, 100).WithMessage("Name should be longer than 4 characters");
            RuleFor(user => user.Gender).NotNull().NotEmpty().Length(1, 9).WithMessage("Gender should be less than 10 characters.");
            RuleFor(user => user.RoleId).NotNull().NotEmpty().WithMessage("Must be a valid role.");
        }
    }

}
