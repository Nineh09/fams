﻿using Application.ViewModels.UserViewModels;
using FluentValidation;

namespace WebAPI.Validations.UserValidationModels
{
    public class UserChangeInfoModelValidation : AbstractValidator<UserChangeInfoModel>
    {
        public UserChangeInfoModelValidation() 
        {
            RuleFor(user => user.DateOfBirth).LessThan(DateTime.Today.AddDays(1)).WithMessage("Must be a valid date.");
            RuleFor(user => user.FullName).Length(4, 100).WithMessage("Name should be longer than 4 characters");
            RuleFor(user => user.Gender).Length(1, 9).WithMessage("Gender should be less than 10 characters.");

        }

    }
}
