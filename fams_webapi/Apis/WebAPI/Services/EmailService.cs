﻿using MailKit.Security;
using MimeKit.Text;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Application.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Org.BouncyCastle.Asn1.Ocsp;
using System.Security.Policy;
using System.Security.Claims;
using MailKit.Search;

namespace Application.Services
{
	public class EmailService : IEmailService
	{
        private readonly IUnitOfWork _unitOfWork;

        private readonly string expireTime = "30";

        public EmailService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task SendEmailForgotPassword(string email, string subject, string htmlMessage)
		{
			var emailToSend = new MimeMessage();
			emailToSend.From.Add(new MailboxAddress("F.A Management System", "minhtam250102@gmail.com"));
			emailToSend.To.Add(MailboxAddress.Parse(email));
			emailToSend.Subject = subject;

            var user = await _unitOfWork.UserRepository.GetUserByEmailHash(email);

			//string htmlBody = htmlMessage;
            string htmlBody = $@"<table align=""left"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">
    <tbody>
        <tr>
            <td align=""left"" valign=""top"">
                <table cellspacing=""0"" cellpadding=""0"" style=""width:100%"">
                    <tbody>
                        <tr>
                            <td class=""m_7152119231688956872responsive-td"" valign=""top"" style=""width:100%"">
                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" role=""presentation"" style=""min-width:100%"">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" role=""presentation"" style=""background-color:#e4e4e4;border:0px;min-width:100%"">
                                                    <tbody>
                                                        <tr>
                                                            <td style=""padding:30px 40px"">
                                                                <div style=""text-align:left"">
                                                                    <span style=""font-size:14px;line-height:20px"">
                                                                        <span style=""color:#454545"">Dear {user.FullName},
                                                                            <br>
                                                                            <br>
                                                                            We received a request to change your password from your account.
                                                                            To confirm this request, please click on the following link within 24 hours:
                                                                            <br><br>
                                                                            <a href=""{htmlMessage}"" target=""_blank"">Confirm change password</a> 
                                                                            <br><br>
                                                                            If you do not click this link, your request to change your password will be cancelled after {expireTime} minutes.<br>
                                                                            If you did not request a password change, please ignore this email.<br><br>
                                                                            Best regards,<br><br>
                                                                            Fresher Academy Management<br>
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">				
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" role=""presentation"" style=""background-color:#252525;min-width:100%"">
                                                    <tbody>
                                                        <tr>
                                                            <td style=""padding:30px 30px 0px"">
                                                                <div style=""text-align:center"">
                                                                    <span style=""font-size:11px;line-height:14px"">
                                                                        <span style=""color:#aaaaaa"">
                                                                            This email was sent to <a href=""{email}"" target=""_blank"">{email}</a> 
                                                                            because you've opted in to receive marketing communications from Fresher Academy Management. 
                                                                            <br>
                                                                            Fresher Academy Management, G Floor, F-Town 1 Building, High-tech Park, Tan Phu Ward, District 9, Ho Chi Minh City, Vietnam
                                                                            <br>
                                                                            <br>
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>";

            emailToSend.Body = new TextPart(TextFormat.Html)
			{
				Text = htmlBody
			};

			using (var emailClient = new SmtpClient())
			{
				emailClient.Connect("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
				emailClient.Authenticate("minhtam250102@gmail.com", "ihrhvyplgkonevqy");
				emailClient.Send(emailToSend);
				emailClient.Disconnect(true);
			}
			//return Task.CompletedTask;
		}

        public async Task SendEmailRegister(string email, string subject, string inputPassword)
        {
            var emailToSend = new MimeMessage();
            emailToSend.From.Add(new MailboxAddress("F.A Management System", "minhtam250102@gmail.com"));
            emailToSend.To.Add(MailboxAddress.Parse(email));
            emailToSend.Subject = subject;

            var user = await _unitOfWork.UserRepository.GetUserByEmailHash(email);

            
            string htmlBody = $@"<table align=""left"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">
    <tbody>
        <tr>
            <td align=""left"" valign=""top"">
                <table cellspacing=""0"" cellpadding=""0"" style=""width:100%"">
                    <tbody>
                        <tr>
                            <td class=""m_7152119231688956872responsive-td"" valign=""top"" style=""width:100%"">
                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" role=""presentation"" style=""min-width:100%"">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" role=""presentation"" style=""background-color:#e4e4e4;border:0px;min-width:100%"">
                                                    <tbody>
                                                        <tr>
                                                            <td style=""padding:30px 40px"">
                                                                <div style=""text-align:left"">
                                                                    <span style=""font-size:14px;line-height:20px"">
                                                                        <span style=""color:#454545"">Dear {user.FullName},
                                                                            <br>
                                                                            <br>
                                                                            Your new account has been created. <br>
                                                                            Your login info are: <br>
                                                                            Email: {user.Email} <br><br>
                                                                            Password: {inputPassword} <br><br>
                                                            
                                                                            <br><br>
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">				
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table cellpadding=""0"" cellspacing=""0"" width=""100%"" role=""presentation"" style=""background-color:#252525;min-width:100%"">
                                                    <tbody>
                                                        <tr>
                                                            <td style=""padding:30px 30px 0px"">
                                                                <div style=""text-align:center"">
                                                                    <span style=""font-size:11px;line-height:14px"">
                                                                        <span style=""color:#aaaaaa"">
                                                                            This email was sent to <a href=""{email}"" target=""_blank"">{email}</a> 
                                                                            because you've opted in to receive marketing communications from Fresher Academy Management. 
                                                                            <br>
                                                                            Fresher Academy Management, G Floor, F-Town 1 Building, High-tech Park, Tan Phu Ward, District 9, Ho Chi Minh City, Vietnam
                                                                            <br>
                                                                            <br>
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>";

            emailToSend.Body = new TextPart(TextFormat.Html)
            {
                Text = htmlBody
            };

            using (var emailClient = new SmtpClient())
            {
                emailClient.Connect("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
                emailClient.Authenticate("minhtam250102@gmail.com", "ihrhvyplgkonevqy");
                emailClient.Send(emailToSend);
                emailClient.Disconnect(true);
            }
            
        }
    }
}
