﻿using Application;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.TokenViewModels;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class TokenController : BaseController
    {
        private readonly ITokenService _tokenService;

        public TokenController(ITokenService tokenService)
        {
            _tokenService = tokenService;
        }

        [HttpPost]
        public async Task<string> GenerateNewTokenFromOldToken(TokenApiModel token) => await _tokenService.GenerateNewTokenFromOld(token);

    }
}
