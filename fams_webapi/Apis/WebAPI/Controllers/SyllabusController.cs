﻿using Application.Interfaces;
using Application.ViewModels.HuanSyllabus;
using Application.ViewModels.SyllabusViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Reflection.Metadata.Ecma335;

namespace WebAPI.Controllers
{
    public class SyllabusController : BaseController
    {

        private readonly ISyllabusService _syllabusService;

        public SyllabusController(ISyllabusService syllabusService)
        {
            _syllabusService = syllabusService;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult GetSyllabus()
        {
            var syllabusDTOList = _syllabusService.GetAllSylla();
            if (syllabusDTOList == null)
            {
                return NotFound();
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(syllabusDTOList);
        }
        [HttpGet("{syllabusSearch}")]
        //[ProducesResponseType(200, Type = typeof(SyllabusListDTO))]
        [ProducesResponseType(200)]        
        [ProducesResponseType(404)]
        public IActionResult GetSyllabusBy(string syllabusSearch)
        {
            var syllabusDTOListBy = _syllabusService.GetSyllabusBy(syllabusSearch);  
            if(syllabusDTOListBy == null || syllabusDTOListBy.Count == 0)
            {
                return NotFound("Syllabus Not Found");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(syllabusDTOListBy);
        }
        [HttpGet("{syllaDetailID}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetSyllabusDetail(int syllaDetailID)
        {
            
                var ae = await  _syllabusService.GetDetailsSyllabus(syllaDetailID);
                if (ae == null)
                {
                    return NotFound("Syllabus Not Found !");
                } 
                if (!ModelState.IsValid)
                {   
                return BadRequest(ModelState);
                }
            return Ok(ae);
                      
        }
        //[HttpGet("{detailID}")]
        //[ProducesResponseType(200)]
        //[ProducesResponseType(204)]
        //[ProducesResponseType(404)]
        //public IActionResult GetSyllabusDetails(int detailID)
        //{
        //    var ae = _syllabusService.GetDetailsSyllabus(detailID);
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //    return Ok(ae);
        //}
        //[HttpDelete("{deleteSyllaId}")]
        //[ProducesResponseType(200)]
        //[ProducesResponseType(204)]
        //[ProducesResponseType(404)]
        //public async Task<IActionResult> DeleteSylla(int deleteSyllaId)
        ////{
        ////    var ae = await _syllabusService.DeleteSyllabus(deleteSyllaId);
        ////    if (!ModelState.IsValid)
        ////    {
        ////        return BadRequest(ModelState);
        ////    }
        ////    return Ok(ae);
        ////}
        [HttpDelete("{deleteReal}")]      
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteSyllabus(int deleteReal)
        {
            try
            {
                await _syllabusService.DeleteSyllaaaa(deleteReal);
                return NoContent();
            } catch (Exception ex)
            {
                return BadRequest(new {error = ex.Message});
            }
        }

        [HttpPost]
        public async Task<string> UpdateSyllabus(int syllabusID, SyllabusUpdateDTO syllabusObject)
        {
            await _syllabusService.UpdateSyllabus(syllabusID, syllabusObject);
            return "Success";
        }

        [HttpPost]
        public async Task<string> CreateSyllabus(CreateSyllabusDTO createSyllabusDTO) 
            => await _syllabusService.CreateSyllabus(createSyllabusDTO);
        
    }
}
