﻿using Application.Interfaces;
using Application.ViewModels.ClassViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
	public class ClassController : BaseController
	{
		private readonly IClassService _classService;

		public ClassController(IClassService classService)
		{
			_classService = classService;
		}

		[HttpPost]
		public async Task<List<ClassDTO>> ViewClassListAsync() => await _classService.ViewClassListAsync();

		[HttpPost]
		public async Task<ClassDetailsDTO> ViewClassDetailsAsync(int classId) => await _classService.ViewClassDetailAsync(classId);

		[HttpGet]
		public async Task<List<ClassDTO>> SearchClassAsync(string keyword) => await _classService.SearchClassAsync(keyword);

		[HttpPost]
		public async Task CreateClassAsync(ClassCreateDTO classCreateDTO) => await _classService.CreateClassAsync(classCreateDTO);
		
		[HttpPut]
		public async Task UpdateClassAsync(ClassUpdateDTO classUpdateDTO) => await _classService.UpdateClassAsync(classUpdateDTO);

		[HttpPut]
		public async Task DeleteClassAsync(int classId) => await _classService.DeleteClassAsync(classId);

		[HttpPost]
		public async Task AddStudentIntoClassAsync(ClassStudentsCreateDTO classStudentsDTO) => await _classService.AddStudentsIntoClassAsync(classStudentsDTO);
	}
}
