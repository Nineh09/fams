using Application.Interfaces;
using Application.ViewModels.TokenModels;
using Application.ViewModels.UserViewModels;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Authorization;

namespace WebAPI.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;

        }

        [AllowAnonymous]
        [HttpGet("GetUsersAsync")]
        public async Task<List<UserViewModel>> GetAllUsers() => await _userService.GetUsersAsync();

        [AllowAnonymous]
        [HttpGet("GetUserByIdAsync")]
        public async Task<UserViewModel> GetUserById(int id) => await _userService.GetUserByIdAsync(id);

        [AllowAnonymous]
        [HttpGet("GetUserByFullNameAsync")]
        public async Task<UserViewModel> GetUserByFullName(string FullName) => await _userService.GetUserByFullNameAsync(FullName);

        [AllowAnonymous]
        [HttpGet("GetUserByEmailAsync")]
        public async Task<UserViewModel> GetUserByEmail(string Email) => await _userService.GetUserByEmailAsync(Email);

        [AllowAnonymous]
        [HttpPost("RegisterAsync")]
        public async Task Register(UserRegisterModel loginObject)
        {
            await _userService.RegisterAsync(loginObject);
            if (!ModelState.IsValid)
            {
                await Task.FromResult(StatusCode(StatusCodes.Status400BadRequest, ModelState));
            }
            await Task.FromResult(StatusCode(StatusCodes.Status200OK));
        }


        [AllowAnonymous]
        [HttpPost("LoginAsync")]
        public async Task<AuthenticateResponse> Login(UserLoginDTO loginObject) => await _userService.LoginAsync(loginObject);

        [AllowAnonymous]
        [HttpPut("ChangePasswordAsync")]
        public async Task ChangePassword(UserChangePasswordModel userObject) => await _userService.ChangePasswordAsync(userObject);

        [AllowAnonymous]
        [HttpPut("ChangeInfoAsync")]
        public async Task ChangeInfo(int id, UserChangeInfoModel userObject) => await _userService.ChangeInfoAsync(id, userObject);

        [AllowAnonymous]
        [HttpPost("SendEmailAsync")]
        public async Task SendEmail(UserForgotPasswordModel model) => await _userService.SendEmailAsync(model);


        [AllowAnonymous]
        [HttpGet("ForgotPasswordAsync")]
        public async Task<string> ForgotPassword(string email, string password, bool confirm, long unixTime)
        {
            var response = await _userService.ForgotPasswordAsync(email, password, confirm, unixTime);
            return response;
        }
    }
}