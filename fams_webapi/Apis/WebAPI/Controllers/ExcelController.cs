﻿using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;

namespace WebAPI.Controllers
{
    public class ExcelController : BaseController
    {
        [HttpPost]
        public IActionResult ImportExcelFile([FromForm] IFormFile file)
        {
            if (file == null || file.Length == 0)
            {
                return BadRequest("No file is selected.");
            }

            using (var stream = new MemoryStream())
            {
                file.CopyTo(stream);

                using (var package = new ExcelPackage(stream))
                {
                    var worksheet = package.Workbook.Worksheets[0]; // Assuming the data is in the first worksheet

                    // Process the data from the Excel worksheet
                    // For example, you can iterate through rows and columns and store the data in your database or perform other operations.

                    // Example:
                    // for (int row = 2; row <= worksheet.Dimension.End.Row; row++)
                    // {
                    //     var value = worksheet.Cells[row, 1].Text; // Access cell value
                    //     // Process and save data as needed
                    // }
                }
            }

            return Ok("Excel file imported successfully.");
        }
    }
}

