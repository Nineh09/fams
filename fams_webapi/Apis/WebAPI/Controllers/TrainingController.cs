using Application.Commons;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.TrainingViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class TrainingController : BaseController
    {
        private readonly ITrainingService _trainingService;

        public TrainingController(ITrainingService traingService)
        {
            _trainingService = traingService;
        }
        [HttpPost]
        public async Task<Pagination<Training>> ViewTrainingListAsync(int pageIndex = 1, int pageSize = 10) => await _trainingService.ViewTrainingListAsync(pageIndex, pageSize);
        [HttpPost]
        public async Task<ViewProgramDetailsDTO> ViewProgramDetailsAsync(int id) => await _trainingService.ViewProgramDetailsAsync(id);
        [HttpGet]
        public async Task<List<Training>> SearchTrainingByNameAsync(string searchValue) => await _trainingService.SearchTrainingByNameAsync(searchValue);
        [HttpPost]
        public async Task DeleteTrainingByIdAsync(int id) => await _trainingService.DeleteTrainingByIdAsync(id);
        [HttpPost]
        public async Task<TrainingListDTO?> CreateTraining(CreateTrainingDTO training)
        {
            return await _trainingService.CreateTrainingAsync(training);
        }
        [HttpPut]
        public async Task UpdateTrainingAsync(int programId, UpdateTrainingDTO training)
        {
            await _trainingService.UpdateTrainingAsync(programId, training);
        }

        [HttpPost]               
        public async Task<IActionResult> ImportTraining(IFormFile formFile)
        {
            if (formFile == null)
            {
                return BadRequest();
            }
            return Ok(await _trainingService.ImportTraining(formFile));
        }
    }
}
