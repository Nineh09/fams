﻿using Application.Interfaces;
using Application.ViewModels.TokenViewModels;

namespace Application.Services
{
    public class TokenService : ITokenService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TokenService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<string> GenerateNewTokenFromOld(TokenApiModel token)
        {
            var principal = _unitOfWork.TokenRepository.GetPrincipalFromOldToken(token.AccessToken);
            return Task.FromResult(_unitOfWork.TokenRepository.GenerateAccessToken(principal.Claims));
        }
    }
}
