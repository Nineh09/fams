﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.HuanSyllabus;
using Application.ViewModels.SyllabusViewModels;
using AutoMapper;
using Domain.Entities;

namespace Application.Services
{
    public class SyllabusService : ISyllabusService
    { 
        private readonly string SuccessString = "Success";

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly AppConfiguration _configuration;
        public SyllabusService(IUnitOfWork unitOfWork, 
            IMapper mapper, ICurrentTime currentTime, 
            AppConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }
        public async Task DeleteSyllaaaa(int id)
        {
            var item = await _unitOfWork.SyllabusRepository.GetIdByAsync(id);
            if(item is null)
            {
                throw new Exception("Not Found");
            }
            await _unitOfWork.SyllabusRepository.DeleteSyllaaa(item);
        }
        public List<SyllabusListDTOO> GetAllSylla()
        {
            var getList = _mapper.Map<List<SyllabusListDTOO>>
                (_unitOfWork.SyllabusRepository.GetSyllabusList());
            if(getList == null) {
                throw new Exception("Empty !");
            }
            return getList;
        }
        public async Task<SyllabusDetailDTO> GetDetailsSyllabus(int syllaDetailID)
        {
            var getListDetail = await _unitOfWork.SyllabusRepository.GetSyllabusDetail(syllaDetailID);
            if(getListDetail == null)
            {
                return null;
            }
            return _mapper.Map<SyllabusDetailDTO>(getListDetail);
        }
        public List<SyllabusListDTOO> GetSyllabusBy(string syllabus)
        {
            var getListBy = _mapper.Map<List<SyllabusListDTOO>>
                (_unitOfWork.SyllabusRepository.GetSyllabusListBy(syllabus));
            if(getListBy == null)
            {
                return null;
            }
            return getListBy;

            // day la comment
        }

        public async Task UpdateSyllabus(int syllabusID, SyllabusUpdateDTO syllabusObject)
        {


            var Syllabus = await _unitOfWork.SyllabusRepository.GetSyllabusByID(syllabusID);
            if (Syllabus is null)
            {
                throw new Exception("Input invalid please try again!");
            }
            Syllabus.SyllabusName = syllabusObject.SyllabusName;
            Syllabus.SyllabusAudience = syllabusObject.SyllabusAudience;
            Syllabus.SyllabusStatus = syllabusObject.SyllabusStatus;
            Syllabus.SyllabusCode = syllabusObject.SyllabusCode;
            Syllabus.SyllabusDate = syllabusObject.SyllabusDate;
            Syllabus.SyllabusBy = syllabusObject.SyllabusBy;
            Syllabus.Duration = syllabusObject.Duration;
            Syllabus.Description = syllabusObject.Description;
            Syllabus.SyllabusAssessment = syllabusObject.SyllabusAssessment;
            
            await _unitOfWork.SaveChangeAsync();

        }

        public async Task<string> CreateSyllabus(CreateSyllabusDTO createSyllabusDTO)
        {
            var existed = await _unitOfWork.SyllabusRepository.CheckCodeExited(createSyllabusDTO.SyllabusCode);
            if (existed) return "Already exists";
            var newSyllabus = new Syllabus
            {
                SyllabusName = createSyllabusDTO.SyllabusName,
                SyllabusCode = createSyllabusDTO.SyllabusCode,
                SyllabusDate = createSyllabusDTO.SyllabusDate,
                SyllabusBy = createSyllabusDTO.SyllabusBy,
                Duration = createSyllabusDTO.Duration,
                Description = createSyllabusDTO.Description,
                SyllabusOutline = createSyllabusDTO.SyllabusOutline,
                SyllabusAudience = createSyllabusDTO.SyllabusAudience,
                SyllabusStatus = createSyllabusDTO.SyllabusStatus,
                SyllabusAssessment = createSyllabusDTO.SyllabusAssessment,

            };
            await _unitOfWork.SyllabusRepository.AddAsync(newSyllabus);
            await _unitOfWork.SaveChangeAsync();
            return "";
        }
        

    }
}
