﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.ClassViewModels;
using AutoMapper;
using Domain.Entities;
using System.Text;

namespace Application.Services
{
    public class ClassService : IClassService
	{
		private readonly IUnitOfWork _unitOfWork;
		private readonly IMapper _mapper;
		private readonly ICurrentTime _currentTime;
		private readonly AppConfiguration _configuration;

		public ClassService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime, AppConfiguration configuration)
		{
			_unitOfWork = unitOfWork;
			_mapper = mapper;
			_currentTime = currentTime;
			_configuration = configuration;
		}
		public async Task<List<ClassDTO>> ViewClassListAsync()
		{
			var classList =  await _unitOfWork.ClassRepository.GetAllAsync();
			var classListDto = new List<ClassDTO>();
			foreach (var item in classList)
			{
				var classListModel = new ClassDTO()
				{
					ClassId = item.ClassId,
					ClassName = item.ClassName,
					ClassCode = item.ClassCode,
					CreatedOn = item.CreatedOn,
					CreateBy = item.CreatedBy,
					Duration = item.Duration,
					Location = item.Location,
					FSU = item.FSU,
					IsDelete = item.IsDeleted
				};

				classListDto.Add(classListModel);
			}

			return classListDto;
		}

		public async Task<ClassDetailsDTO> ViewClassDetailAsync(int classId)
		{
			var classInfo = await _unitOfWork.ClassRepository.GetClassByIdAsync(classId);
			var programId = classInfo.ProgramID;
			var training = await _unitOfWork.ClassRepository.GetTrainingByIdAsync(programId);
			var syllabusList = await _unitOfWork.ClassRepository.GetSyllabusByTrainingIdAsync(programId);
			var studentList = await _unitOfWork.ClassRepository.GetStudentInClassAsync(classId);

			var classDetailsDto = new ClassDetailsDTO()
			{
				ClassId = classId,
				ClassTime = classInfo.ClassTime,
				Localtion = classInfo.LocationDetail,
				Trainer = classInfo.Trainer,
				Admin = classInfo.Admin,
				FSU = classInfo.FSU,
				CreateOn = classInfo.CreatedOn,
				CreateBy = classInfo.CreatedBy,
				TrainingProgram = new ClassTrainingListDTO
				{
					TrainingId = training.ProgramID,
					ProgramName = training.TrainingName,
					Duration = training.Duration,
					CreatedBy = training.CreatedBy
				},
				Syllabus = syllabusList.Select(syllabus => new ClassSyllabusListDTO
				{
					SyllabusId = syllabus.SyllabusID,
					SyllabusName = syllabus.SyllabusName,
					SyllabusStatus = syllabus.SyllabusStatus,
					Duration = syllabus.Duration,
					SyllabusDate = syllabus.SyllabusDate,
					SyllabusBy = syllabus.SyllabusBy
				}).ToList(),
				ClassStudent = studentList.Select(student => new ClassStudentsListDTO
				{
					//ClassId = student.ClassId,
					StudentId = student.UserId
				}).ToList(),
				
			};
			
			return classDetailsDto;
		}

		public async Task<List<ClassDTO>> SearchClassAsync(string keyword)
		{
			var searchResult = await _unitOfWork.ClassRepository.GetClassByKeyNameOrCodeAsync(keyword);

			var classListDto = searchResult.Select(item => new ClassDTO
			{
				ClassId = item.ClassId,
				ClassName = item.ClassName,
				ClassCode = item.ClassCode,
				CreatedOn = item.CreatedOn,
				CreateBy = item.CreatedBy,
				Duration = item.Duration,
				Location = item.Location,
				FSU = item.FSU,
				IsDelete = item.IsDeleted
			}).ToList();

			return classListDto;
		}

		public async Task CreateClassAsync(ClassCreateDTO classCreateDTO)
		{
			var isExisted = await _unitOfWork.ClassRepository.CheckClassNameExistedAsync(classCreateDTO.ClassName);
			if (isExisted)
			{
				throw new Exception("Class Name existed please try again");
			}
			var newClass = new Class
			{
				ClassName = classCreateDTO.ClassName,
				ClassCode = GenerateRandomString(50),
				CreatedOn = _currentTime.GetCurrentTime(),
				CreatedBy = classCreateDTO.CreatedBy,
				Duration = classCreateDTO.Duration,
				Location = classCreateDTO.Location,
				FSU = classCreateDTO.FSU,
				ProgramID = classCreateDTO.ProgramID,
				IsDeleted = false,
				ClassTime = classCreateDTO.ClassTime,
				LocationDetail = classCreateDTO.LocationDetail,
				Admin = classCreateDTO.Admin,
				Trainer = classCreateDTO.Trainer
			};

			await _unitOfWork.ClassRepository.AddAsync(newClass);
			await _unitOfWork.SaveChangeAsync();


		}

		public async Task UpdateClassAsync(ClassUpdateDTO classUpdateDTO)
		{
			var existingClassEntity = await _unitOfWork.ClassRepository.CheckClassExistedAsync(classUpdateDTO.ClassId);

			if (existingClassEntity != null)
			{
				existingClassEntity.ClassName = classUpdateDTO.ClassName; 
				existingClassEntity.Duration = classUpdateDTO.Duration;
				existingClassEntity.Location = classUpdateDTO.Location;
				existingClassEntity.FSU = classUpdateDTO.FSU;
				existingClassEntity.ProgramID = classUpdateDTO.ProgramID;
				existingClassEntity.ModificationDate = _currentTime.GetCurrentTime();
				existingClassEntity.ClassTime = classUpdateDTO.ClassTime;
				existingClassEntity.Location = classUpdateDTO.LocationDetail;
				existingClassEntity.Admin = classUpdateDTO.Admin;
				existingClassEntity.Trainer = classUpdateDTO.Trainer;

				await _unitOfWork.SaveChangeAsync();
			}
		}

		public async Task DeleteClassAsync(int classId)
		{
			var clazz = await _unitOfWork.ClassRepository.CheckClassExistedAsync(classId);
			if (clazz is null)
			{
				throw new Exception("Class is not existed");
			}
			else
			{
				_unitOfWork.ClassRepository.SoftRemove(clazz);
				await _unitOfWork.SaveChangeAsync();
			}
		}

		public async Task AddStudentsIntoClassAsync(ClassStudentsCreateDTO dto)
		{
			var clazz = await _unitOfWork.ClassRepository.GetClassByIdAsync(dto.ClassId);

			if (clazz != null)
			{
				foreach (var studentId in dto.StudentId)
				{
					var student = await _unitOfWork.UserRepository.FindUserById(studentId);

					if (student != null)
					{
						bool studentExistsInClass = await _unitOfWork.ClassRepository.CheckStudentExistedInClassAsync(clazz.ClassId, studentId);

						if (!studentExistsInClass)
						{
							var newClassStudent = new ClassStudents
							{
								ClassId = clazz.ClassId,
								UserId = studentId
							};

							await _unitOfWork.ClassRepository.AddClassStudentsAsync(newClassStudent);
						}
						else throw new Exception("Student already exist in class, no more can be added");
					}
				}

				await _unitOfWork.SaveChangeAsync();
			}
		}

		private string GenerateRandomString(int length)
		{
			const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			Random random = new Random();
			var stringBuilder = new StringBuilder(length);

			for (int i = 0; i < length; i++)
			{
				int index = random.Next(chars.Length);
				stringBuilder.Append(chars[index]);
			}

			return stringBuilder.ToString();
		}




		//public async Task<Class> GetClassByIdAsync(int classID)
		//{
		//	return await _unitOfWork.ClassRepository.GetClassByIdAsync(classID);
		//}


		//public async Task<ClassDetails> ViewClassDetailsAsync(int classID)
		//{
		//	return await _unitOfWork.ClassRepository.GetClassDetailAsync(classID);
		//}



		//public async Task<Training> GetTrainingByIdAsync(int programId)
		//{
		//	return await _unitOfWork.ClassRepository.GetTrainingByIdAsync(programId);
		//}

		//public async Task<List<Syllabus>> ViewSyllabusListByTrainingIdAsync(int programId)
		//{
		//	return await _unitOfWork.ClassRepository.GetSyllabusByTrainingIdAsync(programId);
		//}
	}
}
