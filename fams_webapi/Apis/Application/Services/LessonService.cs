﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.LessonViewModels;
using AutoMapper;
using Domain.Entities;

namespace Application.Services
{
    public class LessonService : ILessonService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly AppConfiguration _configuration;

        public LessonService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime, AppConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }
        

        
        public async Task<Pagination<Lesson>> ViewLessonListAsync(int pageIndex, int pageSize) => await _unitOfWork.LessonRepository.ToPagination(pageIndex, pageSize);

        public async Task<bool> UpdateLessonAsync(int contentId, UpdateLessonDTO updateLesson)
        {
            var existingTraining = await _unitOfWork.LessonRepository.GetLessonById(contentId);
            if (existingTraining == null)
            {
                throw new Exception("Input invalid please try again!");
            }
            existingTraining.Content = updateLesson.Content;
            existingTraining.Duration = updateLesson.Duration;
            existingTraining.Materials = updateLesson.Materials;
            existingTraining.TrainingFormat = updateLesson.TrainingFormat;
            existingTraining.UnitID = updateLesson.UnitID;
            await _unitOfWork.SaveChangeAsync();

            return true;


        }
        public async Task DeleteLessonByIdAsync(int contentId)
        {
            var lesson = await _unitOfWork.LessonRepository.GetLessonById(contentId);
            lesson.IsDeleted = true;
            _unitOfWork.LessonRepository.Update(lesson);
            await _unitOfWork.SaveChangeAsync();
        }

        public async Task CreateLessonAsync(CreateLessonDTO lesson)
        {
            var isExisted = await _unitOfWork.LessonRepository.checkContentExisted(lesson.Content);
            if (isExisted)
            {
                throw new Exception("Content is existed please try again");
            }
            var newlesson = new Lesson
            {
                Content = lesson.Content,
                ContentID = lesson.ContentID,
                Materials = lesson.Materials,
                Duration = lesson.Duration,
                TrainingFormat = lesson.TrainingFormat,
                UnitID = lesson.UnitID,
            };
            await _unitOfWork.LessonRepository.AddAsync(newlesson);
            await _unitOfWork.SaveChangeAsync();
        }

    }
}
