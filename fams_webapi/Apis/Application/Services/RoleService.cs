﻿using Application.Commons;
using Application.Interfaces;
using AutoMapper;
using Domain.Entities;

namespace Application.Services
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly AppConfiguration _configuration;

        public RoleService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime, AppConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }

        public async Task<List<Role>> GetAllRoleAsync()
        {
            var roles = await _unitOfWork.RoleRepository.GetAllAsync();
            return roles;
        }

        public async Task<Role> GetRoleAsync(int roleId)
        {
            var role = await _unitOfWork.RoleRepository.FindRoleByIdAsync(roleId);
            return role;
        }
    }
}
