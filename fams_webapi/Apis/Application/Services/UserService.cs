﻿using Application;
using Application.Commons;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.TokenModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using System.Data.SqlTypes;

namespace Infrastructures.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly AppConfiguration _configuration;
        private readonly IEmailService _emailService;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime, AppConfiguration configuration, IEmailService emailService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
            _emailService = emailService;
        }

        public async Task ChangePasswordAsync(UserChangePasswordModel userObject)
        {
            var user = await _unitOfWork.UserRepository.GetUserByEmailAndPasswordHash(userObject.Email, userObject.Password);
            user.Password = userObject.NewPassword.Hash();
            _unitOfWork.UserRepository.Update(user);
            await _unitOfWork.SaveChangeAsync();

        }

        public async Task ChangeInfoAsync(int id, UserChangeInfoModel userObject)
        {
            var user = await _unitOfWork.UserRepository.FindUserById(id);
            if (userObject.FullName != string.Empty)
            {
                user.FullName = userObject.FullName;
            }

            if (userObject.DateOfBirth.Year != 1)
            {
                user.DateOfBirth = userObject.DateOfBirth;
            }

            if (userObject.Gender != string.Empty)
            {
                user.Gender = userObject.Gender;
            }

            if (userObject.RoleId != 0)
            {
                user.RoleId = userObject.RoleId;
            }
            _unitOfWork.UserRepository.Update(user);
            await _unitOfWork.SaveChangeAsync();

        }




        public async Task<UserViewModel> GetUserByIdAsync(int id)
        {
            var user = await _unitOfWork.UserRepository.FindUserById(id);
            var result = _mapper.Map<UserViewModel>(user);
            return result;
        }

        public User GetUserById(int id)
        {
            var result = _unitOfWork.UserRepository.FindUserById(id);
            return result.GetAwaiter().GetResult();
        }

        public async Task<UserViewModel> GetUserByFullNameAsync(string FullName)
        {
            var user = await _unitOfWork.UserRepository.FindUserByFullName(FullName);
            var result = _mapper.Map<UserViewModel>(user);
            return result;
        }


        public async Task<UserViewModel> GetUserByEmailAsync(string Email)
        {
            var user = await _unitOfWork.UserRepository.FindUserByEmail(Email);
            var result = _mapper.Map<UserViewModel>(user);
            return result;
        }


        public async Task<List<UserViewModel>> GetUsersAsync()
        {

            var users = await _unitOfWork.UserRepository.GetAllAsync();
            var result = _mapper.Map<List<UserViewModel>>(users);
            return result;


        }




        public async Task<AuthenticateResponse> LoginAsync(UserLoginDTO userObject)
        {

            var user = await _unitOfWork.UserRepository.GetUserByEmailAndPasswordHash(userObject.Email, userObject.Password);
            var accessToken = user.GenerateJsonWebToken(_configuration.JWTSecretKey, _currentTime.GetCurrentTime());

            //If RefToken expiry date is smaller than current date, create new refresh token and assign new expiry date
            if (user.RefreshTokenExpiryDate < _currentTime.GetCurrentTime()
                || user.RefreshTokenExpiryDate is null || user.RefreshToken is null)
            {
                var refreshToken = _unitOfWork.TokenRepository.GenerateRefreshToken();

                user.RefreshToken = refreshToken;
                user.RefreshTokenExpiryDate = _currentTime.GetCurrentTime().AddDays(3);
                _unitOfWork.UserRepository.Update(user);
                await _unitOfWork.SaveChangeAsync();
            }

            return new AuthenticateResponse(user, accessToken);

        }

        public async Task RegisterAsync(UserRegisterModel userObject)
        {
            // check username exited
            var isExited = await _unitOfWork.UserRepository.CheckEmailExited(userObject.Email);

            if (isExited)
            {
                throw new ApplicationException("Email existed please try again");
            }
            var role = await _unitOfWork.RoleRepository.FindRoleByIdAsync(userObject.RoleId);

            var newUser = new User
            {
                Role = role,
                RoleId = userObject.RoleId,
                Email = userObject.Email,
                Password = userObject.Password.Hash(),
                FullName = userObject.FullName,
                DateOfBirth = userObject.DateOfBirth,
                Gender = userObject.Gender,
                Level = 1,
                Status = "Ongoing",
            };

            await _unitOfWork.UserRepository.AddAsync(newUser);
            await _unitOfWork.SaveChangeAsync();

            await _emailService.SendEmailRegister(userObject.Email, "New Account", userObject.Password);

        }

        public async Task SendEmailAsync(UserForgotPasswordModel model)
        {
            if (model is null)
            {
                throw new ApplicationException("Can't be null.");
            }

            var user = await _unitOfWork.UserRepository.FindUserByEmail(model.Email);
            if (user is null)
            {
                throw new KeyNotFoundException("User does not exist.");
            }

            var a = _currentTime.GetCurrentUnixTime(_currentTime.GetCurrentTime());

            var url = $"email={model.Email}&password={model.Password}&confirm=true&unixTime={a}";
            //Send Email here (with the url above) ex: https://localhost:5001/User/ForgotPasswordAsync? + "url"

            var urlString = $"https://localhost:5001/User/ForgotPasswordAsync?{url}";
            await _emailService.SendEmailForgotPassword(model.Email, "Forgot password", urlString);
        }

        public async Task<string> ForgotPasswordAsync(string email, string password, bool confirm, long unixTime)
        {
            var user = await _unitOfWork.UserRepository.FindUserByEmail(email);
            user.Password = password.Hash();

            var a = _currentTime.GetCurrentUnixTime(_currentTime.GetCurrentTime());

            if (a - unixTime > 1800000)
            {
                return "Request Expired";
            }


            if (!confirm)
            {
                return "Not Authorized";
            }

            

            await _unitOfWork.SaveChangeAsync();
            return "Success.";
        }
    }
}
