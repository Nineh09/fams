﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.TrainingViewModels;
using AutoMapper;
using Domain.Entities;
using ExcelDataReader;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Data;
using System.Text;

namespace Application.Services
{

    public class TrainingService : ITrainingService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly AppConfiguration _configuration;

        public TrainingService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime, AppConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }

        public async Task DeleteTrainingByIdAsync(int id)
        {
            var trainingProgram = await _unitOfWork.TrainingRepository.GetTrainingById(id);
            trainingProgram.IsDeleted = true;
            _unitOfWork.TrainingRepository.Update(trainingProgram);
            await _unitOfWork.SaveChangeAsync();
        }

        public async Task<List<Training>> SearchTrainingByNameAsync(string searchValue) => await _unitOfWork.TrainingRepository.GetTrainingByName(searchValue);


        public async Task<Pagination<Training>> ViewTrainingListAsync(int pageIndex, int pageSize) => await _unitOfWork.TrainingRepository.ToPagination(pageIndex, pageSize);
        public async Task<ViewProgramDetailsDTO> ViewProgramDetailsAsync(int id) => await _unitOfWork.TrainingRepository.GetProgramDetails(id);


        public async Task<TrainingListDTO?> CreateTrainingAsync(CreateTrainingDTO training)
        {
            var isExisted = await _unitOfWork.TrainingRepository.CheckTrainingNameExisted(training.TrainingName);           
            var traininglObj = _mapper.Map<Training>(training);
            await _unitOfWork.TrainingRepository.AddAsync(traininglObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (isSuccess)
            {
                return _mapper.Map<TrainingListDTO>(traininglObj);
            }
            return null;
        }

        



        public async Task<bool> UpdateTrainingAsync(int Id, UpdateTrainingDTO updateTraining)
        {
            var existingTraining = await _unitOfWork.TrainingRepository.GetTrainingById(Id);
            
            existingTraining.TrainingName = updateTraining.TrainingName;
            existingTraining.TrainingDate = updateTraining.TrainingDate;
            existingTraining.Duration = updateTraining.Duration;
            existingTraining.CreatedBy = updateTraining.CreatedBy;
            existingTraining.CreatedDate = updateTraining.CreatedDate;
            existingTraining.Status = updateTraining.Status;
            await _unitOfWork.SaveChangeAsync();

            return true;
        }
        public async Task<string> ImportTraining(IFormFile formFile)
        {
            var trainingParsing = new List<TrainingExcelDTO>();
            var dataTable = ReadFile(formFile);
            foreach (var dataRow in dataTable.AsEnumerable())
            {
                var customerRow = new TrainingExcelDTO()
                {
                    ProgramID = Convert.ToInt32(dataRow.ItemArray[0]?.ToString()),
                    TrainingName = dataRow.ItemArray[1]?.ToString(),
                    Duration = dataRow.ItemArray[2]?.ToString(),
                    Status = dataRow.ItemArray[3]?.ToString(),
                    CreatedBy = dataRow.ItemArray[4]?.ToString(),

                };
                trainingParsing.Add(customerRow);
            }
            return JsonConvert.SerializeObject(trainingParsing);
        }

        public async Task<List<Training>> ImportTrainingToObject(IFormFile formFile)
        {
            var trainingConverting = new List<Training>();
            var dataTable = ReadFile(formFile);
            foreach (var dataRow in dataTable.AsEnumerable())
            {
                var training = new Training()
                {
                    ProgramID = Convert.ToInt32(dataRow.ItemArray[0]?.ToString()),
                    TrainingName = dataRow.ItemArray[1]?.ToString(),
                    Duration = dataRow.ItemArray[2]?.ToString(),
                    Status = dataRow.ItemArray[3]?.ToString(),
                    CreatedBy = dataRow.ItemArray[4]?.ToString(),
                };
                trainingConverting.Add(training);

            }
            return trainingConverting;
        }

        private DataTable ReadFile(IFormFile formFile)
        {
            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);
                stream.Position = 0;

                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var dataSetConfig = new ExcelDataSetConfiguration
                    {
                        ConfigureDataTable = a => new ExcelDataTableConfiguration
                        {
                            UseHeaderRow = true,
                        }
                    };

                    return reader.AsDataSet(dataSetConfig).Tables[0];
                }
            }
        }


    }





}
