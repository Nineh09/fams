﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.HuanSyllabus;
using AutoMapper;
using Domain.Entities;

namespace Application.Services
{
    public class UnitService : IUnitService
    {
        private readonly string SuccessString = "Success";

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly AppConfiguration _configuration;

        public UnitService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime, AppConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }

        public Task CreateLesson(Lesson lesson)
        {
            throw new NotImplementedException();
        }

        public async Task CreateUnit(CreateUnitDTO createUnitDTO)
        {
            var updateUnit = await _unitOfWork.UnitRepository.GetSyllabusID(createUnitDTO.SyllabusID);
            var newUnit = new Unit
            { 
                UnitName = createUnitDTO.UnitName,
                Description = createUnitDTO.Description,
                NumberLesson = createUnitDTO.NumberLesson,
                SyllabusID = createUnitDTO.SyllabusID,
            };
            await _unitOfWork.UnitRepository.AddAsync(newUnit);
            await _unitOfWork.SaveChangeAsync();
            
        }

        public async Task UpdateUnit(int UnitID, CreateUnitDTO updateUnitObject)
        {
            var updateUnit = await _unitOfWork.UnitRepository.GetUnitID(UnitID);
            if(updateUnit is null)
            {
                throw new Exception("Input invalid please try again!");
            }
            updateUnit.UnitName = updateUnitObject.UnitName;
            updateUnit.Description = updateUnitObject.Description;
            updateUnit.NumberLesson = updateUnitObject.NumberLesson;
            updateUnit.SyllabusID = updateUnitObject.SyllabusID;


            await _unitOfWork.SaveChangeAsync();
        }

        public async Task DeleteUnit(int UnitID)
        {
            var deleteUnit = await _unitOfWork.UnitRepository.GetUnitID(UnitID);
            deleteUnit.IsDeleted = true;
            _unitOfWork.UnitRepository.Update(deleteUnit);
            await _unitOfWork.SaveChangeAsync();
        }
    }
}
