﻿using Application.Repositories;

namespace Application
{
    public interface IUnitOfWork
    {
        //public IChemicalRepository ChemicalRepository { get; }

        public IUserRepository UserRepository { get; }

        public IRoleRepository RoleRepository { get; }

        public ITokenRepository TokenRepository { get; }

        public ITrainingRepository TrainingRepository { get; }
        public ILessonRepository LessonRepository { get; }
        public IProgramDetailRepository ProgramDetailRepository { get; }

        public IClassRepository ClassRepository { get; }

        public ISyllabusRepository SyllabusRepository { get; }

        public IUnitRepository UnitRepository { get; }


        public Task<int> SaveChangeAsync();
    }
}
