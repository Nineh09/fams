﻿using Domain.Entities;


namespace Application.Repositories
{
    public interface IUnitRepository : IGenericRepository<Unit>
    {
        public Task<Unit> GetSyllabusID(int SyllabusID);

        public Task<Unit> GetUnitID(int UnitID);
        
    }
}
