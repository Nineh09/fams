﻿using Application.ViewModels.SyllabusViewModels;
using Domain.Entities;

namespace Application.Repositories
{
    public interface ISyllabusRepository : IGenericRepository<Syllabus>
    {
        
        List<Syllabus> GetSyllabusList();
        List<Syllabus> GetSyllabusListBy(string input);
        Task<SyllabusDetailDTO> GetSyllabusDetail(int id);
        
        Task DeleteSyllaaa(Syllabus syllabus);
        Task<Syllabus> GetIdByAsync(int id);

        Task<Syllabus> GetSyllabusByID(int syllabusID);


        bool Save();
        
        public Task<bool> CheckCodeExited(string code);
        

    }
}
