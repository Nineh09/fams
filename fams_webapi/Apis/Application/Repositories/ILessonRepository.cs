﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface ILessonRepository : IGenericRepository<Lesson>
    {
        Task<Lesson> GetLessonById(int contentId);

        Task<bool> checkContentExisted(string content);

        Task<Lesson?> checkContentIdExisted(int contentId);
    }
}
