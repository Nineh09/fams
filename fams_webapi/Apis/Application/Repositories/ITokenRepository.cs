﻿using System.Security.Claims;

namespace Application.Repositories
{
    public interface ITokenRepository
    {
        public string GenerateAccessToken(IEnumerable<Claim> claims);
        public string GenerateRefreshToken();
        public ClaimsPrincipal GetPrincipalFromOldToken(string token);
    }
}
