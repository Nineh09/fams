﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ProgramDetailViewModel
{
    public class CreateProgramDetailDTO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProgramDetailId { get; set; }
        public int SyllabusID { get; set; }
        public int ProgramID { get; set; }
        [StringLength(50)]
        public string? Sequence { get; set; }
    }
}
