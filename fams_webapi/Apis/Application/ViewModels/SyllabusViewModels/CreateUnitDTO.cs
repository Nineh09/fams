﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.HuanSyllabus
{
    public class CreateUnitDTO
    {
        public string UnitName { get; set; }
        public string Description { get; set; }
        public int NumberLesson { get; set; }
        public int SyllabusID { get; set; }
        
    }
}
