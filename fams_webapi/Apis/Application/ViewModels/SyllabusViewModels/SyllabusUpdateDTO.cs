﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.SyllabusViewModels
{
    public class SyllabusUpdateDTO
    {
        public int SyllabusID { get; set; }
        public string SyllabusName { get; set; }
        public int? SyllabusAudience { get; set; }
        public string SyllabusStatus { get; set; }
        public string SyllabusCode { get; set; }
        public DateTime SyllabusDate { get; set; }
        public string SyllabusBy { get; set; }
        public string Duration { get; set; }
        public string Description { get; set; }
        public string SyllabusAssessment { get; set; } = string.Empty;
    }
}
