﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TrainingViewModels
{
    public class TrainingExcelDTO
    {
        public int ProgramID { get; set; }
        public string TrainingName { get; set; }
        public string Duration { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        
        
        
    }
}
