﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TokenModels
{
    public class AuthenticateResponse
    {
        public int Id { get; set; }
        public string? Email { get; set; }
        public string? AccessToken { get; set; }
        public string? RefreshToken { get; set; }
        public DateTime RefreshTokenExpiryDate { get; set; }


        public AuthenticateResponse(User user, string token)
        {
            Id = user.UserId;
            Email = user.Email;
            AccessToken = token;
            RefreshToken = user.RefreshToken;
            RefreshTokenExpiryDate = user.RefreshTokenExpiryDate.Value;
        }
    }
}