﻿using Domain.Entities;

namespace Application.ViewModels.UserViewModels
{
    public class UserViewModel
    {
        //public string _Id { get; set; } = string.Empty;
        public int UserID { get; set; }
        public int RoleID { get; set; }
        public string Email { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public string FullName { get; set; } = string.Empty;
        public string DateOfBirth { get; set; } = string.Empty; 
        public string Gender { get; set; } = string.Empty;
        public int Level { get; set; }
        public string Status { get; set; } = string.Empty;
        //public Role? Role { get; set; }


    }
}
