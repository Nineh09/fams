﻿using Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace Application.ViewModels.ClassViewModels
{
    public class ClassUpdateDTO
    {
        public int ClassId { get; set; }

		public string ClassName { get; set; }

		public string Duration { get; set; }

        public string Location { get; set; }

        public string FSU { get; set; }

        public int ProgramID { get; set; }

		public string ClassTime { get; set; }

		public string LocationDetail { get; set; }

		public string Admin { get; set; }

		public string Trainer { get; set; }

	}
}
