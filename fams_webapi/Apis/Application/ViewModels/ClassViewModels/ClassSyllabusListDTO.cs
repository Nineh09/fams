﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ClassViewModels
{
    public class ClassSyllabusListDTO
    {
		public int SyllabusId { get; set; }

        public string SyllabusName { get; set; }

		public string SyllabusStatus { get; set; }

		public string Duration { get; set; }

		public DateTime SyllabusDate { get; set; }

		public string SyllabusBy { get; set; }
	}
}
