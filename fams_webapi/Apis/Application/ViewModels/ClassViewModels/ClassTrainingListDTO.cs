﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ClassViewModels
{
    public class ClassTrainingListDTO
    {
		public int TrainingId { get; set; }
        public string ProgramName { get; set; }
		public string Duration { get; set; }
		public string CreatedBy { get; set; }
	}
}
