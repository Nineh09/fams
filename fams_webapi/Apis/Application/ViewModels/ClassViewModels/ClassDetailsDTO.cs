﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ClassViewModels
{
    public class ClassDetailsDTO
	{
		public int ClassId { get; set; }
		public string ClassTime { get; set; }
		public string Localtion { get; set; }
		public string Trainer { get; set; }
		public string Admin { get; set; }
		public string FSU { get; set; }
		public DateTime CreateOn { get; set; }
		public string CreateBy { get; set; }
		public ClassTrainingListDTO TrainingProgram { get; set; }
		public List<ClassSyllabusListDTO> Syllabus { get; set; }
		public List<ClassStudentsListDTO> ClassStudent { get; set; }
	}
}
