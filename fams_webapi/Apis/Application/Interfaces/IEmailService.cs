﻿namespace Application.Interfaces
{
    public interface IEmailService
    {
        public Task SendEmailForgotPassword(string email, string subject, string htmlMessage);

        public Task SendEmailRegister(string email, string subject, string inputPassword);
    }
}
