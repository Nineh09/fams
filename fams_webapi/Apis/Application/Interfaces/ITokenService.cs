﻿using Application.ViewModels.TokenViewModels;

namespace Application.Interfaces
{
    public interface ITokenService
    {
        public Task<string> GenerateNewTokenFromOld(TokenApiModel token);
    }
}
