﻿using Application.ViewModels.TokenModels;
using Application.ViewModels.UserViewModels;
using Domain.Entities;

namespace Application.Interfaces
{
    public interface IUserService
    {

        public Task<List<UserViewModel>> GetUsersAsync();
        public Task<UserViewModel> GetUserByIdAsync(int id);
        public User GetUserById(int id);
        public Task<string> ForgotPasswordAsync(string email, string password, bool confirm, long unixTime);
        public Task<UserViewModel> GetUserByFullNameAsync(string FullName);
        public Task<UserViewModel> GetUserByEmailAsync(string Email);
        public Task RegisterAsync(UserRegisterModel userObject);
        public Task<AuthenticateResponse> LoginAsync(UserLoginDTO userObject);
        public Task ChangePasswordAsync(UserChangePasswordModel userObject);
        public Task ChangeInfoAsync(int id, UserChangeInfoModel userObject);
        public Task SendEmailAsync(UserForgotPasswordModel model);

    }
}
