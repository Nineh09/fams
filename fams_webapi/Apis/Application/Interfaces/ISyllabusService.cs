﻿using Application.ViewModels.HuanSyllabus;
using Application.ViewModels.SyllabusViewModels;

namespace Application.Interfaces
{
    public interface ISyllabusService
    {
        List<SyllabusListDTOO> GetAllSylla();
        Task<SyllabusDetailDTO> GetDetailsSyllabus(int syllaDetailID);
        List<SyllabusListDTOO> GetSyllabusBy(string syllabus);       
        Task DeleteSyllaaaa(int id);

        Task UpdateSyllabus(int syllabusID, SyllabusUpdateDTO syllabus);

        public Task<string> CreateSyllabus(CreateSyllabusDTO createSyllabusDTO);
        
    }
}
