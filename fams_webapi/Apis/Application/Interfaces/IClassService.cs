﻿using Application.ViewModels.ClassViewModels;

namespace Application.Interfaces
{
    public interface IClassService
	{
		public Task<List<ClassDTO>> ViewClassListAsync();

		public Task<ClassDetailsDTO> ViewClassDetailAsync(int classId);

		public Task<List<ClassDTO>> SearchClassAsync(string keyword);

		public Task CreateClassAsync(ClassCreateDTO classNameObject);

		public Task UpdateClassAsync(ClassUpdateDTO classNameObject);

		public Task DeleteClassAsync(int classId);

		public Task AddStudentsIntoClassAsync(ClassStudentsCreateDTO dto);
	}
}
