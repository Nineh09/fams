﻿using Application.ViewModels.HuanSyllabus;
using Domain.Entities;

namespace Application.Interfaces
{
    public interface IUnitService
    {
        public Task CreateUnit(CreateUnitDTO createUnitObject);
        public Task UpdateUnit(int UnitID, CreateUnitDTO updateUnitObject);
        
        public Task CreateLesson(Lesson lesson);
        Task DeleteUnit(int unitID);
    }
}
