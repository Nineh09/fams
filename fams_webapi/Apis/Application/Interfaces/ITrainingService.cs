﻿using Application.Commons;
using Application.ViewModels.TrainingViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Http;

namespace Application.Interfaces
{
    public interface ITrainingService
    {
        public Task<Pagination<Training>> ViewTrainingListAsync(int pageIndex, int pageSize);
        public Task<List<Training>> SearchTrainingByNameAsync(string searchValue);
        public Task DeleteTrainingByIdAsync(int id);
        public Task<ViewProgramDetailsDTO> ViewProgramDetailsAsync(int id);
        public Task<TrainingListDTO?> CreateTrainingAsync(CreateTrainingDTO training);
        Task<bool> UpdateTrainingAsync(int programId, UpdateTrainingDTO updateTraining);

        public Task<string> ImportTraining(IFormFile formFile);

        public Task<List<Training>> ImportTrainingToObject(IFormFile formFile);
    }
}
