﻿using Application.Commons;
using Application.ViewModels.ProgramDetailViewModel;
using Application.ViewModels.TrainingViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;

namespace Infrastructures.Mappers
{
    public class MapperConfigurationsProfile : Profile
    {

        public MapperConfigurationsProfile()
        {

            CreateMap<CreateUserViewModel, User>();
            CreateMap(typeof(Pagination<>), typeof(Pagination<>));
            CreateMap<User, UserViewModel>()
                //.ForMember(dest => dest._Id, src => src.MapFrom(x => x.Id))
                //.ForMember(dest => dest.Role, src => src.MapFrom(x => x.Role))
                .ForMember(dest => dest.DateOfBirth, src => src.MapFrom(x => x.DateOfBirth.Date.ToShortDateString()));
            CreateMap<CreateTrainingDTO, Training>();
            CreateMap<Training, CreateTrainingDTO>()
                .ForMember(dest => dest.ProgramID, src => src.MapFrom(x => x.Id));
            CreateMap<Training, TrainingListDTO>();
            CreateMap<CreateProgramDetailDTO, ProgramDetails>();
            CreateMap<CreateProgramDetailDTO, ProgramDetails>()
                .ForMember(dest => dest.ProgramDetailId, src => src.MapFrom(m => m.ProgramDetailId));
            CreateMap<CreateProgramDetailDTO, ProgramDetails>();

        }
    }
}
