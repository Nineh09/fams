﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Infrastructures
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<Attendance> Attendances { get; set; }
		public DbSet<ClassStudents> ClassStudents { get; set; }
		public DbSet<Class> Classes { get; set; }
		public DbSet<Training> Trainings { get; set; }
		public DbSet<ProgramDetails> ProgramDetails { get; set; }
		public DbSet<Syllabus> Syllabuses { get; set; }
		public DbSet<Unit> Units { get; set; }
		public DbSet<Lesson> Lessons { get; set; }
	}
}
