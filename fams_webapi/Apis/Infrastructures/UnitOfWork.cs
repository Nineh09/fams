﻿using Application;
using Application.Repositories;
using Infrastructures.Repositories;

namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _dbContext;
        //private readonly IChemicalRepository _chemicalRepository;
        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly ITokenRepository _tokenRepository;
        private readonly ITrainingRepository _trainingRepository;
        private readonly ILessonRepository _lessonRepository;
        private readonly IProgramDetailRepository _programDetailRepository;
        private readonly IClassRepository _classRepository;
        private readonly ISyllabusRepository _syllabusRepository;
        private IUnitRepository _unitRepository;

        public UnitOfWork(AppDbContext dbContext,
            //IChemicalRepository chemicalRepository,
            IUserRepository userRepository,
            ITrainingRepository trainingRepository,
            ILessonRepository lessonRepository,
            IProgramDetailRepository programDetailRepository,
            IClassRepository classRepository,
            ITokenRepository tokenRepository,
            IRoleRepository roleRepository,
            ISyllabusRepository syllabusRepository,
            IUnitRepository unitRepository)
        {
            _dbContext = dbContext;
            //_chemicalRepository = chemicalRepository;
            _userRepository = userRepository;
            _trainingRepository = trainingRepository;
            _lessonRepository = lessonRepository;
            _programDetailRepository = programDetailRepository;
            _classRepository = classRepository;
            _tokenRepository = tokenRepository;
            _roleRepository = roleRepository;
            _syllabusRepository = syllabusRepository;
            _unitRepository = unitRepository;
        }
        //public IChemicalRepository ChemicalRepository => _chemicalRepository;

        public IUserRepository UserRepository => _userRepository;
        public ITrainingRepository TrainingRepository => _trainingRepository;
        public IProgramDetailRepository ProgramDetailRepository => _programDetailRepository;
        public ILessonRepository LessonRepository => _lessonRepository;
        public IClassRepository ClassRepository => _classRepository;

        public IRoleRepository RoleRepository => _roleRepository;

        public ITokenRepository TokenRepository => _tokenRepository;

        public ISyllabusRepository SyllabusRepository => _syllabusRepository;
        public IUnitRepository UnitRepository => _unitRepository;

        public async Task<int> SaveChangeAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }
    }
}
