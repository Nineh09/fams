﻿using Application;
using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.LessonViewModels;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class UnitRepository : GenericRepository<Unit>, IUnitRepository
    {
        private readonly AppDbContext _dbContext;
        public UnitRepository(AppDbContext dbContext,
            ICurrentTime timeService,
            IClaimsService claimsService)
                 : base(dbContext,
                  timeService,
                  claimsService)
        {
            _dbContext = dbContext;
        }

        public async Task<Unit> GetSyllabusID(int id)
        {
            var Unit = await _dbContext.Units.FirstOrDefaultAsync(s => s.SyllabusID == id);
            if (Unit is null)
            {
                throw new Exception("Unit does not exists");
            }
            return Unit;
        }

        public async Task<Unit> GetUnitID(int id)
        {
            var Unit = await _dbContext.Units.FirstOrDefaultAsync(u => u.UnitID == id);
            if(Unit is null)
            {
                throw new Exception("UnitID " + id + "does not exists");
            }
            return Unit;
        }
    }
}
