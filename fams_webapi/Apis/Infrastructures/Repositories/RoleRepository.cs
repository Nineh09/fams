﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;


namespace Infrastructures.Repositories
{
    public class RoleRepository : GenericRepository<Role>, IRoleRepository
    {
        private readonly AppDbContext _dbContext;

        public RoleRepository(AppDbContext context, ICurrentTime timeService, IClaimsService claimsService) : base(context, timeService, claimsService)
        {
            _dbContext = context;
        }

        public Task<Role?> FindRoleByIdAsync(int roleId)
        {
            var role = _dbContext.Roles.FirstOrDefaultAsync(u => u.RoleID == roleId);

            if(role is null) {
                throw new Exception("Does not exists. ");
            }

            return role;
        }

    }
}
