﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
	public class ClassRepository : GenericRepository<Class>, IClassRepository
	{
		private readonly AppDbContext _dbContext;

		public ClassRepository(AppDbContext dbContext,
			ICurrentTime timeService,
			IClaimsService claimsService)
			: base(dbContext,
				  timeService,
				  claimsService)
		{
			_dbContext = dbContext;
		}

		public async Task<Class> GetClassByIdAsync(int classId)
		{
			var result = await _dbContext.Classes.FirstOrDefaultAsync(x => x.ClassId == classId);
			if (result is null)
			{
				throw new Exception("Class does not existed");
			}
			return result;
		}

		public async Task<Training> GetTrainingByIdAsync(int programId)
		{
			var result = await _dbContext.Trainings.FirstOrDefaultAsync(x => x.ProgramID == programId);
			if (result is null)
			{
				throw new Exception("Training does not existed");
			}
			return result;
		}

		public async Task<List<Syllabus>> GetSyllabusByTrainingIdAsync(int programId)
		{
			var programDetails = await _dbContext.ProgramDetails.Where(x => x.ProgramID == programId).ToListAsync();

			List<int> syllabusIds = programDetails.Select(x => x.SyllabusID).ToList();

			var syllabusList = await _dbContext.Syllabuses.Where(x => syllabusIds.Contains(x.SyllabusID)).ToListAsync();

			return syllabusList;
		}

		public async Task<List<Class>> GetClassByKeyNameOrCodeAsync(string keyword)
		{
			var searchResults = await _dbContext.Classes.Where(x => x.ClassName.Contains(keyword) || x.ClassCode.Contains(keyword)).ToListAsync();
			if (searchResults is null)
			{
				throw new Exception($"{keyword} does not existed");
			}

			return searchResults;
		}

		public async Task<bool> CheckClassNameExistedAsync(string className) => await _dbContext.Classes.AnyAsync(c => c.ClassName == className);
		
		public async Task<bool> CheckClassCodeExistedAsync(string code) => await _dbContext.Classes.AnyAsync(c => c.ClassCode == code);
		
		public async Task<Class?> CheckClassExistedAsync(int classId)
		{
			var result = await _dbContext.Classes.FirstOrDefaultAsync(c => c.ClassId == classId);
			if (result is null)
			{
				throw new Exception("Class does not existed");
			}

			return result;
		}
			

		public async Task AddClassStudentsAsync(ClassStudents classStudents)
		{
			await _dbContext.ClassStudents.AddAsync(classStudents);
			await _dbContext.SaveChangesAsync();
		}

		public async Task<bool> CheckStudentExistedInClassAsync(int classId, int userId) => await _dbContext.ClassStudents.AnyAsync(c => c.ClassId == classId && c.UserId == userId);

		public async Task<List<ClassStudents>> GetStudentInClassAsync(int classId)
		{
			var result = await _dbContext.ClassStudents.Where(x => x.ClassId == classId).ToListAsync();
			if (result is null)
			{
				throw new Exception("Class does not existed.");
			}
			return result;
		}

		//public async Task<List<Class>> GetAllClass()
		//{
		//	return await _dbContext.Classes.ToListAsync();
		//}

	}
}
