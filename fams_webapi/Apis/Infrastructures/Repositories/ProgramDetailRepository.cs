﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class ProgramDetailRepository : GenericRepository<ProgramDetails>, IProgramDetailRepository
    {
        private readonly AppDbContext _dbContext;
        public ProgramDetailRepository(AppDbContext dbContext,
            ICurrentTime timeService,
            IClaimsService claimsService)
                 : base(dbContext,
                  timeService,
                  claimsService)
        {
            _dbContext = dbContext;
        }
        public async Task<ProgramDetails> GetProgDById(int programDetailId)
        {
            var progd = await _dbContext.ProgramDetails.FirstOrDefaultAsync(record => record.ProgramDetailId == programDetailId);

            if (progd is null)
            {
                throw new Exception("Content with id: " + programDetailId + " doesn't exist");
            }
            return progd;
        }

        
    }
}
