﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class LessonRepository :GenericRepository<Lesson>, ILessonRepository
    {
        private readonly AppDbContext _dbContext;
        public LessonRepository(AppDbContext dbContext,
            ICurrentTime timeService,
            IClaimsService claimsService)
                 : base(dbContext,
                  timeService,
                  claimsService)
        {
            _dbContext = dbContext;
        }

        public Task<bool> checkContentExisted(string content) => _dbContext.Lessons.AnyAsync(lc => lc.Content == content);    
        

        public Task<Lesson?> checkContentIdExisted(int contentId) => _dbContext.Lessons.FirstOrDefaultAsync(l => l.ContentID == contentId);
        
        
        public async Task<Lesson> GetLessonById(int contentId)
        {
            var lesson = await _dbContext.Lessons.FirstOrDefaultAsync(record => record.ContentID == contentId);

            if (lesson is null)
            {
                throw new Exception("Content with id: " + contentId + " doesn't exist");
            }
            return lesson;
        }

        
    }
}
