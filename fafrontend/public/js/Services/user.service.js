import { apiUrl } from "./_endpoint.service.js";
const endpoint = `${apiUrl}/User`;

const loginUser = async (model) => {
  const response = await $.ajax({
    url: `${endpoint}/Login`,
    type: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    data: model ? JSON.stringify(model) : undefined,
  });

  return response;
};
const register = async (model) => {
  const response = await $.ajax({
    url: `${endpoint}/Register`,
    type: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    data: model ? JSON.stringify(model) : undefined,
  });

  return response;
};
const googleLogin = async (model) => {
  const response = await $.ajax({
    url: `${endpoint}/Google-Login`,
    type: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    data: model ? JSON.stringify(model) : undefined,
  });
  return response;
};

export {loginUser, register, googleLogin }