import { apiUrl } from "./_endpoint.service.js";

const endpoint = `${apiUrl}/TrainingProgram`;

const getTrainingProgram = async () => {
  const response = await $.ajax({
    url: `${endpoint}/Get-All`,
    type: "GET",
  });
  return response;
};

export { getTrainingProgram };
